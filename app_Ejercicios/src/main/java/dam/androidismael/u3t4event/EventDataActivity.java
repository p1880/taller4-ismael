package dam.androidismael.u3t4event;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;

public class EventDataActivity extends AppCompatActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private TextView tvEventName;
    private EditText etPlace;
    private RadioGroup rgPriority;
    private DatePicker dpDate;
    private TimePicker tpTime;
    private Button btAccept;
    private Button btCancel;
    private int priority = R.string.rbNormal;
    private Bundle inputData;
    private Bundle inputDataCancel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_data);
        setUi();
        inputData = getIntent().getExtras();
        if(inputData.getBoolean("bool")){
            escribir();
            inputDataCancel = inputData;
        }

    }

    private void escribir(){
        tvEventName.setText(inputData.getString("EventName"));
        etPlace.setText(inputData.getString("Place"));
        rgPriority.check(onChecked(inputData.getInt("Priority")));
        dpDate.updateDate(inputData.getInt("Year"), inputData.getInt("Month"), inputData.getInt("Day"));
        tpTime.setHour(inputData.getInt("Hour"));
        tpTime.setMinute(inputData.getInt("Min"));

    }

    private void setUi(){
        tvEventName = (TextView)  findViewById(R.id.tvEventName);
        rgPriority = (RadioGroup) findViewById(R.id.rgPriority);
        rgPriority.check(R.id.rbNormal);
        dpDate = (DatePicker) findViewById(R.id.dpDate);
        tpTime = (TimePicker) findViewById(R.id.tpTime);
        tpTime.setIs24HourView(true);
        btAccept = (Button) findViewById(R.id.btAccept);
        btCancel = (Button) findViewById(R.id.btCancel);
        etPlace = (EditText) findViewById(R.id.etPlaceName);

        btAccept.setOnClickListener(this);
        btCancel.setOnClickListener(this);
        rgPriority.setOnCheckedChangeListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent activityResult = new Intent();
        Bundle eventData = new Bundle();

        switch (view.getId()){
            case R.id.btAccept:
                eventData.putInt("Priority", priority);
                eventData.putInt("Month",  dpDate.getMonth());
                eventData.putInt("Day", dpDate.getDayOfMonth());
                eventData.putInt("Year", dpDate.getYear());
                eventData.putString("Place", etPlace.getText().toString());
                eventData.putInt("Hour", tpTime.getHour());
                eventData.putInt("Min", tpTime.getMinute());
                activityResult.putExtras(eventData);
                break;
            case R.id.btCancel:
                activityResult.putExtras(inputDataCancel);
                break;
        }
        setResult(RESULT_OK, activityResult);

        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        switch (i){
            case R.id.rbLow:
                priority = R.string.rbLow;
                break;
            case R.id.rbNormal:
                priority = R.string.rbNormal;
                break;
            case R.id.rbHigh:
                priority = R.string.rbHigh;
                break;
        }
    }
    public int onChecked( int i) {
        switch (i){
            case R.string.rbLow:
                return R.id.rbLow;
            case R.string.rbNormal:
                return R.id.rbNormal;
            case R.string.rbHigh:
                return R.id.rbHigh;
            default:
                return 0;
        }
    }
}