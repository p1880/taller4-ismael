package dam.androidismael.u3t4event;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private EditText etEventName;
    private TextView tvCurrentData;
    private static final String DEBUG_TAG = "LOG-";
    private int priority;
    private String place;
    private int day;
    private int month;
    private int year;
    private int hour;
    private int min;
    private Bundle bundle;
    private boolean bool = false;


    private ActivityResultLauncher<Intent> eventActivityResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
        registerForEventResult();
    }
    private void setUI(){
        etEventName = (EditText) findViewById(R.id.etEventName);
        tvCurrentData = (TextView) findViewById(R.id.tvCurrentData);
    }

    public void editEventData(View v) {
        Intent intent = new Intent(this, EventDataActivity.class);
        bundle = new Bundle();
        bundle.putString("EventName", etEventName.getText().toString());
        bundle.putInt("Priority", priority);
        bundle.putString("Place", place);
        bundle.putInt("Day", day);
        bundle.putInt("Month", month);
        bundle.putInt("Year", year);
        bundle.putInt("Hour", hour);
        bundle.putInt("Min", min);
        bundle.putBoolean("bool", bool);
        intent.putExtras(bundle);
        intent.putExtra("tvCurrentData", tvCurrentData.getText());
        eventActivityResult.launch(intent);
    }

    private void registerForEventResult() {
        eventActivityResult = registerForActivityResult (new ActivityResultContracts.StartActivityForResult(), new ActivityResultCallback<ActivityResult>() {
            @Override
            public void onActivityResult(ActivityResult result) {
                if(result.getResultCode() == RESULT_OK){
                    Intent data = result.getData();

                    if(data != null){
                        Resources res = getResources();
                        String[] months = res.getStringArray(R.array.month);
                        String pririty = onCheckedChanged(data.getIntExtra("Priority", 1));
                        String texto = res.getString(R.string.tvPlace) + ": " + data.getStringExtra("Place") + "\n" +
                                res.getString(R.string.tvPriority) + ": " + pririty + "\n" +
                                res.getString(R.string.lbDate) + ": " + data.getIntExtra("Day",1) + " " + months[data.getIntExtra("Month", 1)] + " " + data.getIntExtra("Year",1) + "\n" +
                                res.getString(R.string.lbHour) + ": " + data.getIntExtra("Hour", 1) + ":" + data.getIntExtra("Min", 1);
                        tvCurrentData.setText(texto);
                        priority = data.getIntExtra("Priority", 1);
                        place = data.getStringExtra("Place");
                        day = data.getIntExtra("Day",1);
                        month = data.getIntExtra("Month",1);
                        year = data.getIntExtra("Year",1);
                        hour = data.getIntExtra("Hour",1);
                        min = data.getIntExtra("Min",1);
                        bool = true;
                    }
                }
            }
        });

    }
    public String onCheckedChanged( int i) {
        switch (i){
            case R.string.rbLow:
                return getResources().getString(R.string.rbLow);
            case R.string.rbNormal:
                return getResources().getString(R.string.rbNormal);
            case R.string.rbHigh:
                return getResources().getString(R.string.rbHigh);
            default:
                return "";
        }
    }

    protected void onRestoreInstanceState(Bundle savedInstanceState){
        super.onRestoreInstanceState(savedInstanceState);
        tvCurrentData.setText(savedInstanceState.getString("tvCurrentData"));
        Log.i(DEBUG_TAG+ getLocalClassName(), " onRestore ");
    }

    protected void onSaveInstanceState(Bundle outState){
        outState.putString("tvCurrentData", tvCurrentData.getText().toString());
        super.onSaveInstanceState(outState);
        Log.i(DEBUG_TAG+ getLocalClassName(), " onSave ");
    }
}